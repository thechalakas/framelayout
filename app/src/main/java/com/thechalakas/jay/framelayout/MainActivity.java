package com.thechalakas.jay.framelayout;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import java.net.URL;

/*
 * Created by jay on 15/09/17. 11:38 AM
 * https://www.linkedin.com/in/thesanguinetrainer/
 * https://bitbucket.org/account/user/thechalakas/projects/DROID
 */



public class MainActivity extends AppCompatActivity
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        WebView webView = (WebView) findViewById(R.id.WebView1);
        //change this to whatever url you want
        String urlstring = "http://thechalakas.com";
        // a webview on its own cannot show a website. you need to add a client to it.
        WebViewClient webViewClient = new WebViewClient();
        webView.setWebViewClient(webViewClient);
        //enable javascript because all sites use js these days
        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadUrl(urlstring);

    }
}
